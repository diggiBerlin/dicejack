package com.volkening;

import java.util.Scanner;

/**
 * This is the Computerplayer class which inherits from Playerinterface.
 * It is the opponent in the game.
 * @author Henrik Volkening
 * @version 0.1
 */
public class ComputerPlayer extends Player implements PlayerInterface {

    /**
     * Constructor, sets playersname right at the beginning.
     */
    public ComputerPlayer() {

        this.setName("Computer");
    }


    /**
     * Lets the computer chose the next playing state.
     * @param scanner System Scanner
     * @return Playerstate
     */
    @Override
    public PlayerState choseNextState(Scanner scanner) {

        state = PlayerState.ENDED;
        String returnValue = "N";

        if (getDescisionOfPlaying()) {
            state = PlayerState.PLAYING;
            returnValue = "Y";
        }

        System.out.println(returnValue);
        return state;
    }

    /**
     * overrides the print of current sum of eyes,
     * becauase the computer does not wnt to shoe the sum to the player.
     */
    @Override
    public void printSumOfEyes() {

        System.out.println("\nI am the Computer, i will not show my sum!");
    }

    /**
     * overrides the print of the last roll,
     * becuase the computer does not want to shoe the last roll to the player.
     */
    @Override
    public void printLastRoll() {

        System.out.println("Roll the dice: Don't look!");
    }

    /**
     * Simple AI. Desides, if the computer should roll the dice again.
     * It will alwways return true (play again) until the factorOfProbability in <= 1.
     * Then it will randomly return a "play again" with descending probability.
     * @return descision (true = play again, false = don't play again)
     */
    private boolean getDescisionOfPlaying(){

        double factorOfProbability = (double) (DiceJackMain.MAXEYES - this.getNumberOfEyes()) / Die.NUMBER_OF_SIDES;
        double randomValue = Math.random();

        return (randomValue < factorOfProbability);
    }
}