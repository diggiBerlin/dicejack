package com.volkening;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Created by henrik on 18.12.16.
 */
public class PlayerTest extends TestCase {
    public void testSetAndGetName() throws Exception {
        String name = "äöüßÄÄöü817236g";
        Player testPlayer = new Player();
        testPlayer.setName(name);
        String value = testPlayer.getName();
        Assert.assertEquals(value, name);
    }

}