package com.volkening;

import java.util.Scanner;

/**
 * Player Class. This represents the player with name, die, state and outputs.
 */
public class Player implements PlayerInterface {
    private Die die;
    private String playerName;
    protected PlayerState state;
    private int numberOfEyes = 0;

    
    /**
     * Constructor: creates a die for the player and sets the playing-status.
     */
    public Player() {
        this.die = new Die();
        this.state = PlayerState.PLAYING;
    }

    /**
     * shows current playing state
     * @return the current playing state
     */
    public PlayerState getState() {
        return state;
    }

    /**
     * shows the players name.
     * @return the players name
     */
    public String getName() {
        return playerName;
    }

    /**
     * sets the players name
     * @param name the name of the player
     */
    public void setName(String name) {
        if (name != null && !name.trim().isEmpty())
            this.playerName = name;
    }

    /**
     * shows the current Number of collected eyes.
     * @return number of collected eyes
     */
    public int getNumberOfEyes() {
        return numberOfEyes;
    }

    /**
     * sets the number of eyes to a value.
     * @param eyes number of eyes
     */
    public void setNumberOfEyes(int eyes) {
            this.numberOfEyes = eyes;
    }

    /**
     * shows the number of eyes from the last dice roll
     * @return number of eyes
     */
    public int getLastEyes(){
        return die.getEyes();
    }

    /**
     * Adds the number of eyes to to current number
     * @param eyes numbers of eyes to add
     */
    public void addNumberOfEyes(int eyes){
        int sum = this.getNumberOfEyes() + eyes;
        this.setNumberOfEyes(sum);
    }


    /**
     * Plays one round. Rolls the dice and prints out the last roll.
     */
    //@Override
    public void play() {
        die.roll();
        int eyes = die.getEyes();
        addNumberOfEyes(eyes);
        printLastRoll();
    }

    /**
     * Lets the Player chose, if he/she want's to play another round.
     * Uses keyboard
     * @param scanner System Scanner
     * @return Playerstate
     */
    //@Override
    public PlayerState choseNextState(Scanner scanner) {
        while (true) {
            char in = Character.toUpperCase(scanner.next().charAt(0));
            if (in == 'Y') {
                state = PlayerState.PLAYING;
                return state;
            } else if (in == 'N') {
                state = PlayerState.ENDED;
                return state;
            } else {
                System.out.print("That is no valid input. Enter Y or N");
            }
        }
    }


    /**
     * prints Players name to console
     */
    //@Override
    public void printPlayerName() {
        System.out.printf("\n%s", this.getName());
    }

    /**
     * prints current sum of eyes to the console
     */
    //@Override
    public void printSumOfEyes() {
        System.out.printf("\nYour sum is now %d.\n", this.getNumberOfEyes());
    }

    /**
     * prints the last roll to console
     */
    //@Override
    public void printLastRoll() {
        System.out.printf("\nRoll the dice: %d Eyes!", this.getLastEyes());
    }

    /**
     * prints a combined output of playersname and
     * players results to console
     */
    //@Override
    public void printOutPlayerResults(){
        printPlayerName();
        printSumOfEyes();
    }


}
