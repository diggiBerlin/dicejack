package com.volkening;

import java.util.Scanner;

/**
 * Players Interface with methods to override for player classes
 */
public interface PlayerInterface {

    void play();
    void printSumOfEyes();
    void printLastRoll();
    void printPlayerName();
    void printOutPlayerResults();
    PlayerState choseNextState(Scanner scanner);
}