package com.volkening;

import java.util.Random;

/**
 * Created by henrik on 26.11.16.
 */
public class Die {

    public static final int NUMBER_OF_SIDES = 6;
    private int eyes;

    /**
     * Constructor: Do an initial roll!
     */
    Die() {

        roll();
    }

    /**
     * The roll method sets the eyes to a random value with a maximum of NUMBER_OF_SIDES.
     */
    @Diceroll
    public void roll() {

        Random randomValue = new Random();
        eyes = randomValue.nextInt(NUMBER_OF_SIDES) + 1;
    }


    /**
     * Get current eyes of the die (the ones that are on top)
     *
     * @return number
     */
    public int getEyes() {

        return this.eyes;
    }
}
