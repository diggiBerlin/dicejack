package com.volkening;

import java.util.*;

/**
 * This is a dice variation of blackjack
 * for the console
 *
 * @author Henrik Volkening
 * @version 0.1
 */
public class DiceJackMain {

    static final int MAXEYES = 21;
    private static Scanner systemScannerIn = new Scanner(System.in);    // input Scanner
    private ArrayList<Player> playersList = new ArrayList<Player>();
    private GamePhase currentPhase;                                     // the current Gamephase

    private DiceJackMain() {

        displayIntroText();
        initGame();
        playGame();
        showResults();
        showWinner();
    }

    /**
     * Main method. Just initiates the constructor
     *
     * @param args
     */
    public static void main(String[] args) {

        DiceJackMain game = new DiceJackMain();
        System.out.println("Thanks for playing!");
    }

    /**
     * Introtext with instructions how to play the game
     */
    public static void displayIntroText() {
        System.out.println("This is a dice variation of blackjack.\n");
        System.out.printf("The goal of this game is to get to %d as close as you can.\n", MAXEYES);
        System.out.println("If you get more eyes, you have lost!\n"
                + "Every round You can roll a dice.\n"
                + "The eyes will be added to the current sum.\n"
                + "You can chose to stop playing at any time.\n"
                + "Your opponent is a highly intelligent computer AI!\n"
                + "The starter will be chosen randomly!\n\n");
    }


    /**
     * Inits the game. The human player has to enter her/his name.
     * The Players are stored in an Arraylist.
     * The Arraylist will be shuffled to set a random starter.
     */
    private void initGame() {

        System.out.println("\nPlease enter your name:");
        String playerName = systemScannerIn.next();
        Player humanPlayer = new Player();
        humanPlayer.setName(playerName);
        playersList.add(humanPlayer);

        ComputerPlayer computerPlayer = new ComputerPlayer();
        playersList.add(computerPlayer);

        Collections.shuffle(playersList);
        currentPhase = GamePhase.PLAYING;
    }


    /**
     * The playGame Method plays every turn until both players request to stop rolling the dice.
     */
    private void playGame() {
        do {
            boolean stillPlaying = false;
            for (Player player : playersList) {
                if (player.getState() == PlayerState.PLAYING) {

                    System.out.println("\n--------------- NEXT PLAYER! ---------------");
                    player.printOutPlayerResults();
                    System.out.println("Do you want to play another round?");

                    PlayerState newPlayerState = player.choseNextState(systemScannerIn);
                    if (newPlayerState == PlayerState.PLAYING) {
                        player.play();
                        stillPlaying = true;
                    }

                } else {
                    System.out.printf("Player %s stopped playing.\n", player.getName());
                }
            }

            if (!stillPlaying) {
                currentPhase = GamePhase.END;
                System.out.println("Game has ended!");
            }

        } while (currentPhase == GamePhase.PLAYING);
    }


    /**
     * Prints out the playerresults of each player.
     */
    private void showResults() {

        for (Player player : playersList) {
            player.printOutPlayerResults();
        }
    }

    /**
     * calls the calculation of the winner and prints out the results.
     */
    private void showWinner() {

        ArrayList<Player> winnersList = getWinnerList();

        if (winnersList.isEmpty()) {
            System.out.printf("Sorry No Winner!\n");
        } else {
            for (Player player : winnersList) {
                System.out.printf("Player %s HAS WON! Your sum is %d.\n", player.getName(), player.getNumberOfEyes());
            }
        }

    }

    /**
     * calculates an Arraylist of the winners.
     *
     * @return List of the winning players
     */
    private ArrayList<Player> getWinnerList() {

        int winnersEyes = 0;
        ArrayList<Player> winnersList = new ArrayList();

        for (Player player : playersList) {
            int playersEyes = player.getNumberOfEyes();
            if (playersEyes <= MAXEYES) {
                if (winnersList.isEmpty()) {
                    winnersList.add(player);
                    winnersEyes = playersEyes;
                } else {
                    if (playersEyes > winnersEyes) {
                        winnersList.clear();
                    }
                    if (playersEyes >= winnersEyes) {
                        winnersList.add(player);
                    }
                }
            }
        }
        return winnersList;
    }

}