package com.volkening;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;


/**
 * Class to print out values of rolled dices.
 * Using AspectJ and Annotations for a better handling.
 * learned from: https://blog.jayway.com/2015/09/08/defining-pointcuts-by-annotations/
 */
@Aspect
public class AnnotationDiceThrowlogger {
    /**
     * Defines a pointcut where the @Diceroll annotation exists
     * and the code is being executed.
     * Because we want to get the rolled Eyes of the Die Class, we additionally
     * combined the pointcut with the target die.
     *
     * ProceedingJointPoint is the reference of the call to the method.
     * @param joinPoint: ProceedingJointPoint is needed for the @Around advice, because it can be procceeded.
     * @param die: Only handle dice
     *
     */
    @Around("@annotation(Diceroll) && execution(* *(..)) && target(die)")
    public Object aroundDiceRoll(ProceedingJoinPoint joinPoint, Die die) throws Throwable {

        //Default Object that we can use to return to the consumer
        Object returnObject = null;

        try {
            System.out.printf("AnnotationDiceThrowlogger Before: Old Throw: Value %s\n", die.getEyes());
            returnObject = joinPoint.proceed();

        } catch (Throwable throwable) {
            throw throwable;

        }
        finally {
            System.out.printf("AnnotationDiceThrowlogger After: New Throw: Value %s\n", die.getEyes());

        }
        return returnObject;
    }
}