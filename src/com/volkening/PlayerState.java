package com.volkening;

/**
 * Created by henrik on 26.11.16.
 */

/**
 * Enumerations for player states
 */
public enum PlayerState {
    PLAYING, ENDED
}
