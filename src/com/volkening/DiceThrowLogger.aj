package com.volkening;


/**
 * AspectJ Aspect to print out values of rolled dices.
 *
 */
public aspect DiceThrowLogger {

    pointcut diceRollExecuted() : execution (* com.volkening.Die.roll(..));

    before(): diceRollExecuted(){
        Object target = thisJoinPoint.getTarget();
        if (target instanceof Die) {
            System.out.printf("DiceThrowLogger Old Throw Before: Value %s\n", ((Die) target).getEyes());
        }
    }


    after(): diceRollExecuted() {
        Object target = thisJoinPoint.getTarget();
        if (target instanceof Die) {
            System.out.printf("DiceThrowLogger New Throw After: Value %s\n", ((Die) target).getEyes());
        }
    }


}